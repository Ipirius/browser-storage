import Cookie, {CookieAttributes} from 'js-cookie'
import {decodeData, encodeData} from "../utils";

class CookieStorage {
    set(key: string, data: string | {}, opts?: CookieAttributes) {
        Cookie.set(key, encodeData(data), opts)
    }

    get(key: string): string {
        const data = Cookie.get(key) || ''
        return decodeData(data)
    }

    remove(key: string): void {
        Cookie.remove(key)
    }
}

export default new CookieStorage()
