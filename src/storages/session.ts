import {decodeData, encodeData, isBrowser} from "../utils";

class SessionStorage {
    private readonly storage
    private readonly memoryStorage
    constructor() {
        this.memoryStorage = new Map()
        this.storage = isBrowser() && window.sessionStorage
    }

    set(key: string, data: string | {}) {
        this.storage ? this.storage.setItem(key, encodeData(data)) : this.memoryStorage.set(key, data)
    }

    get(key: string) {
        if (this.storage) {
            const data = this.storage.getItem(key)
            return data ? decodeData(data) : null
        }
        return this.memoryStorage.get(key)
    }

    remove(key: string) {
        this.storage ? this.storage.removeItem(key) : this.memoryStorage.delete(key)
    }
}

export default new SessionStorage()
