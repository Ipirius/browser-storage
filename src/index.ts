import cookie from './storages/cookie'
import local from './storages/local'
import session from './storages/session'

export type StoreTypes = 'cookie' | 'local' | 'session'

type Payload = {
    type: StoreTypes,
    data: string | {}
    options?: {}
    key: string
}

class Store {
    private cookie = cookie
    private local = local
    private session = session

    set({ type, data, key, options }: Payload): void {
        this[type].set(key, data, options)
    }

    get({ type, key }: Omit<Payload, 'data'>): string {
        return this[type].get(key)
    }

    remove({ type, key }: Omit<Payload, 'data'> ): void {
        this[type].remove(key)
    }
}

export default new Store()
