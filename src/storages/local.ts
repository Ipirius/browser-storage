import {decodeData, encodeData, isBrowser} from "../utils";

class LocalStorage {
    private readonly storage
    constructor() {
        this.storage = isBrowser() && window.localStorage
        this.get = this.storageCheck(this.get)
        this.set = this.storageCheck(this.set)
        this.remove = this.storageCheck(this.remove)
    }

    storageCheck(fn: any) {
        if (!this.storage) {
            console.warn('Local storage is unavailable')
            return () => {}
        }
        return (...params: any) => {
            return fn(params)
        }
    }

    get(key: string): any {
        // @ts-ignore
        const data = this.storage.getItem(key)
        return data ? decodeData(data) : null
    }

    set(key: string, data: string | {}): void {
        // @ts-ignore
        this.storage.setItem(key, encodeData(data))
    }

    remove(key: string): void {
        // @ts-ignore
        this.storage.removeItem(key)
    }
}

export default new LocalStorage()
