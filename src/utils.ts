export const encodeData = (data: string | {}) => {
    if (typeof data === 'string') return data
    return JSON.stringify(data)
}

export const decodeData = (data: string) => {
    try {
        return JSON.parse(data)
    } catch {
        return data
    }
}

export const isBrowser = () => typeof window !== 'undefined' && typeof window.document !== 'undefined'
